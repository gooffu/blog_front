import Vue from "vue";
import VueRouter from "vue-router";

import BlogMain from "@/components/BlogMain";
import BlogDetail from "@/components/BlogDetail";

const router = new VueRouter ({
    routes: [
        {
            path: "/",
            component: BlogMain,
        },
        {
            path: "/:id",
            component:BlogDetail,
        }
    ]
})

const App = new Vue({
    router
}).$mount('#App')